import webapp2
import logging
from google.appengine.api import xmpp
from boilerplate import models
from boilerplate.lib import utils, captcha, twitter
from ..models import UrlDocket, InvalidUrlError, DuplicateUrlError
import sys

APP_URL = "http://hyperlinklet.appspot.com"

class XMPPHandler(webapp2.RequestHandler):
    def post(self):
        message = xmpp.Message(self.request.POST)
        sender = self.request.get('from').split('/')[0]

        user = None
        if sender != '' and utils.is_email_valid(sender):
            user = models.User.get_by_email(sender)

        if user is None:
            message.reply("Sign-up in 10 seconds at %s using google." % APP_URL)
            return

        body = message.body
        try:
            if body.find("\n") != -1:
                urls = body.split("\n")
                for url in urls:
                    url_obj = UrlDocket.New(url, user)
            else:
                url = body
                url_obj = UrlDocket.New(url, user)

            message.reply("Saved.")

        except InvalidUrlError as e:
            message.reply("Could not save. http:// prefix missing perhaps?")
        except DuplicateUrlError as e:
            message.reply("Url already saved. Edit here: %s/links/edit/%s" % (APP_URL, e))
