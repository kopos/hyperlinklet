from xmpp_handler import XMPPHandler
from links_handler import LinksHandler
from link_handler import LinkHandler, EditLinkHandler
from tags_handler import TagsHandler
from tag_handler import TagHandler, EditTagHandler
from dashboard_handler import DashboardHandler
