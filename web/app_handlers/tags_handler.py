import webapp2
import logging
from boilerplate.lib.basehandler import *
from ..models import Tag

"""
" This class lists all the tags owned by a user
"""
class TagsHandler(BaseHandler):
    def get(self):
        params = {'tags': [] }

        if self.user:
            user = models.User.get_by_id(long(self.user_id))
            q = Tag.query(Tag.whocreated == user.key).order(-Tag.whencreated)
            for tag in q:
                params['tags'].append(tag)
            self.render_template('app_show_tags.html', **params)
        else:
            self.redirect('/home')

    def post(self):
        if not self.user:
            self.redirect('/home')

        logging.info(str(self.request.POST))
