import webapp2
import logging
from boilerplate.lib.basehandler import *
from ..models import UrlDocket, Tag

"""
" This class lists all the details of all the links owned by a user.
" A UrlDocket / Link has a 
" - url
" - tldr
" - tags
"""
class LinksHandler(BaseHandler):
    def get(self):
        params = { 'urls': [] }
        if self.user:
            user = models.User.get_by_id(long(self.user_id))

            q = UrlDocket.query(UrlDocket.whocreated == user.key).order(-UrlDocket.whencreated)

            for url in q:
                url.labels = [Tag.get_by_id(tag.id()).name for tag in url.tags]
                params['urls'].append(url)

            self.render_template('app_show_links.html', **params)
        else:
            self.redirect('/home')

    def post(self):
        if not self.user:
            self.redirect('Home')
