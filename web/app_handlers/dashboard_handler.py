import webapp2
import logging
from boilerplate.lib.basehandler import *
from ..models import UrlDocket
from ..models import Tag

class DashboardHandler(BaseHandler):
    def get(self):
        params = {}
        if self.user:
            self.render_template('app_dashboard.html', **params)
        else:
            self.redirect('/home')

    def post(self):
        if not self.user:
            self.redirect('Home')

        logging.info(str(self.request.POST))
