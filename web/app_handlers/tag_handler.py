import webapp2
import logging
from boilerplate.lib.basehandler import *
from ..models import Tag, UrlDocket
from .. import forms

class TagHandler(BaseHandler):
    def get(self, tag_name, action):
        params = { 'urls': [] }

        if self.user:
            user = models.User.get_by_id(long(self.user_id))
            params['label'] = tag_name

            tq = Tag.query(Tag.name == tag_name, Tag.whocreated == user.key)
            tag = tq.get()

            q = UrlDocket.query(UrlDocket.tags.IN([tag.key]), UrlDocket.whocreated == user.key).order(-UrlDocket.whencreated)
            for url in q:
                params['urls'].append(url)

            self.render_template('app_show_tag.html', **params)
        else:
            self.redirect('/home')

    def post(self):
        if not self.user:
            self.redirect('/home')

        logging.info(str(self.request.POST))


class EditTagHandler(BaseHandler):
    def get(self, tag_name):
        params = {}

        if self.user:
            user = models.User.get_by_id(long(self.user_id))
            tq = Tag.query(Tag.name == tag_name, Tag.whocreated == user.key)
            tag = tq.get()

            self.form.tag_name.data = tag.name
            self.form.tag_desc.data = tag.desc
            self.form.tag_meta.data = tag.meta

            self.render_template('app_edit_tag.html', **params)
        else:
            self.redirect('/tags')

    def post(self, tag_name):
        if not self.form.validate():
            return self.get(tag_name)

        if self.user:
            user = models.User.get_by_id(long(self.user_id))
            tag_name = self.form.tag_name.data
            tag_desc = self.form.tag_desc.data.strip()
            tag_meta = self.form.tag_meta.data.strip()

            message = ''

            tq = Tag.query(Tag.name == tag_name, Tag.whocreated == user.key)
            tag = tq.get()

            tag.name = tag_name
            tag.desc = tag_desc
            tag.meta = tag_meta
            tag.put()

            message += " " + 'Thanks, your tag has been updated.'
            self.add_message(message, 'success')

        return self.get(tag_name)

    @webapp2.cached_property
    def form(self):
        return forms.EditTagForm(self)
