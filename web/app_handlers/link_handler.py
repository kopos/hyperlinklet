import webapp2
import logging
from boilerplate.lib.basehandler import *
from ..models import UrlDocket, Tag
from .. import forms


class LinkHandler(BaseHandler):

    def get(self, link_id, action):
        params = {}

        if self.user:
            user = models.User.get_by_id(long(self.user_id))
            url = UrlDocket.get_by_id(long(link_id))
            labels = [Tag.get_by_id(tag.id()).name for tag in url.tags]

            params['uri'] = url.url
            params['tldr'] = url.tldr
            params['link_id'] = link_id
            params['labels'] = labels

            logging.info(params)

            self.render_template('app_show_link.html', **params)
        else:
            self.redirect('/home')

    def post(self):
        if not self.user:
            self.redirect('Home')


class EditLinkHandler(BaseHandler):

    def get(self, link_id):
        params = {}

        if self.user:
            user = models.User.get_by_id(long(self.user_id))
            url = UrlDocket.get_by_id(long(link_id))
            tags = [Tag.get_by_id(tag.id()).name for tag in url.tags]

            self.form.url.data = url.url
            self.form.tldr.data = url.tldr.replace("<br/>", "\n")
            self.form.tags.data = ",".join(tags)

            self.render_template('app_edit_link.html', **params)
        else:
            self.redirect('/links')

    def post(self, link_id):
        if not self.form.validate():
            return self.get(link_id)

        if self.user:
            user = models.User.get_by_id(long(self.user_id))

            url =  self.form.url.data.strip()
            tldr = self.form.tldr.data.strip().replace("\n", "<br/>")
            tags = self.form.tags.data.strip()

            message = ''
            url_docket = UrlDocket.get_by_id(long(link_id))
            tag_refs = [Tag.query(Tag.name == tag_name.strip(), Tag.whocreated == user.key).get().key for tag_name in tags.split(",")]

            url_docket.url = url
            url_docket.tldr = tldr
            url_docket.tags = tag_refs
            url_docket.put()

            message += " " + 'Thanks, your link has been updated.'
            self.add_message(message, 'success')

        return self.get(link_id)

    @webapp2.cached_property
    def form(self):
        return forms.EditLinkForm(self)
