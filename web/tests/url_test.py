import re

UNREAD, READ, TLDRED, SHARED = 0, 1, 2, 3

class UrlDocket():
#    url = ndb.StringProperty()
#    tldr = ndb.TextProperty()
#    user = ndb.KeyProperty(kind = User)
#    date = ndb.DateTimeProperty()
#    status = ndb.IntegerProperty(choices = [UNREAD, READ, TLDRED, SHARED])
#    tags = ndb.StringProperty(repeated = True)

#    @staticmethod
#    def New(url, user):
#        return UrlDocket(
#            url = url,
#            user = user.key,
#            date = datetime.datetime.now(),
#            status = UNREAD
#        ).put()

    @staticmethod
    def IsUrlDocket(string):
        regex = re.compile(
            r'^(?:http|ftp)s?://'  # http:// or https://
            r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|'  # domain...
            r'localhost|'  # localhost...
            r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}|'  # ...or ipv4
            r'\[?[A-F0-9]*:[A-F0-9:]+\]?)'  # ...or ipv6
            r'(?::\d+)?'  # optional port
            r'(?:/?|[/?]\S+)$', re.IGNORECASE)
        if regex.match(string):
            return True
        return False

    @staticmethod
    def New(string, user):
        url = ''
        tags = []

        tokens = string.split(" ")
        for token in tokens:
            if UrlDocket.IsUrlDocket(token):
                url = token
                continue
            if token[0] == '#':
                tags.append(token[1:])
                continue

        print 'uri: ' + url
        print 'tags: ' + str(tags)

#        return UrlDocket (
#            url = url,
#            user = user.key,
#            date = datetime.datetime.now(),
#            status = UNREAD,
#            tags = tags
#        )
#

if __name__ == "__main__":
    print UrlDocket.New('http://stackoverflow.com/questions/827557/how-do-you-validate-a-url-with-a-regular-expression-in-python #so #code', None)
