from wtforms import fields
from wtforms import Form
from wtforms import validators
from boilerplate.lib import utils
from webapp2_extras.i18n import lazy_gettext as _
from webapp2_extras.i18n import ngettext, gettext

from boilerplate.forms import FormTranslations, BaseForm, FIELD_MAXLENGTH

DESC_FIELD_MAXLENGTH = 200


class EditTagForm(BaseForm):
    tag_name = fields.TextField(_('Tag'), [validators.Required(), validators.Length(max=FIELD_MAXLENGTH), validators.regexp(utils.ALPHANUMERIC_REGEXP, message=_('Tag invalid. Use only letters and numbers.'))])
    tag_desc = fields.TextField(_('Desc'), [validators.Length(max=DESC_FIELD_MAXLENGTH)])
    tag_meta = fields.TextField(_('Meta'), [validators.Length(max=DESC_FIELD_MAXLENGTH)])


class EditLinkForm(BaseForm):
    url = fields.TextField(_('Link'), [validators.Required()])
    tldr = fields.TextAreaField(_('TL;DR'), [validators.Required()])
    tags = fields.TextField(_('Tags'), [validators.Required()])
