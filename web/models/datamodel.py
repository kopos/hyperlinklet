from webapp2_extras.appengine.auth.models import User
from google.appengine.ext import ndb
import datetime
import logging
import re

UNREAD, READ, TLDRED, SHARED = 0, 1, 2, 3

class InvalidUrlError(ValueError):
    pass

class DuplicateUrlError(ValueError):
    pass

class Tag(ndb.Model):
    name = ndb.StringProperty()
    desc = ndb.StringProperty()
    whocreated = ndb.KeyProperty(kind = User)
    meta = ndb.StringProperty()
    whencreated = ndb.DateTimeProperty(auto_now_add = True)
    whenupdated = ndb.DateTimeProperty(auto_now = True)

    @staticmethod
    def IsTagPresent(tag, user):
        try:
            t = Tag.query(Tag.whocreated == user.key, Tag.name == tag).get()

            if t is not None and t.name == tag:
                return t
        except:
            pass

        return None

    @staticmethod
    def New(tag, user):
        t = Tag.IsTagPresent(tag, user)

        if not t:
            t = Tag(name = tag,
                    whocreated = user.key,
                    desc = '',
                    meta = '')
            t.put()

        return t

class UrlDocket(ndb.Model):
    url = ndb.StringProperty()
    tldr = ndb.TextProperty()
    whocreated = ndb.KeyProperty(kind = User)
    status = ndb.IntegerProperty(choices = [UNREAD, READ, TLDRED, SHARED])
    tags = ndb.KeyProperty(kind = Tag, repeated = True)
    whencreated = ndb.DateTimeProperty(auto_now_add = True)
    whenupdated = ndb.DateTimeProperty(auto_now = True)

    @staticmethod
    def IsUrl(string):
        regex = re.compile(
            r'^(?:http|ftp)s?://'  # http:// or https://
            r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|'  # domain...
            r'localhost|'  # localhost...
            r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}|'  # ...or ipv4
            r'\[?[A-F0-9]*:[A-F0-9:]+\]?)'  # ...or ipv6
            r'(?::\d+)?'  # optional port
            r'(?:/?|[/?]\S+)$', re.IGNORECASE)
        if regex.match(string):
            return True
        return False

    @staticmethod
    def IsUrlSaved(url, user):
        u = UrlDocket.query(UrlDocket.whocreated == user.key, UrlDocket.url == url).get()

        if u is not None and u.url == url:
            return u
        return None

    @staticmethod
    def New(string, user):
        link = None
        labels = []
        tags = []

        tokens = string.split(" ")
        for token in tokens:
            if UrlDocket.IsUrl(token):
                link = token
                continue
            if token[0] == '#':
                labels.append(token[1:])
                continue

        u = UrlDocket.IsUrlSaved(link, user)
        if u is not None:
            raise DuplicateUrlError(u.key.id())
        if not link:
            raise InvalidUrlError("no link found in message string")

        tags = [Tag.New(l, user).key for l in labels]
        logging.info(str(tags))

        url = UrlDocket(url = link,
                        whocreated = user.key,
                        status = UNREAD,
                        tags = tags)
        url.put()

        return url
