"""
Using redirect route instead of simple routes since it supports strict_slash
Simple route: http://webapp-improved.appspot.com/guide/routing.html#simple-routes
RedirectRoute: http://webapp-improved.appspot.com/api/webapp2_extras/routes.html#webapp2_extras.routes.RedirectRoute
"""

from webapp2_extras.routes import RedirectRoute
from web import handlers
import rest

secure_scheme = 'https'

_routes = [
    RedirectRoute('/secure/', handlers.SecureRequestHandler, name='secure', strict_slash=True),
    RedirectRoute('/_ah/xmpp/message/chat/', handlers.XMPPHandler, name='im', strict_slash=False),
    RedirectRoute('/links/', handlers.LinksHandler, name='links', strict_slash=True),
    RedirectRoute('/links/<link_id>/edit', handlers.EditLinkHandler, name='edit-link', strict_slash=True),
    RedirectRoute('/links/<link_id>/<action>', handlers.LinkHandler, name='show-link', strict_slash=True),
    RedirectRoute('/tags/', handlers.TagsHandler, name='tags', strict_slash=True),
    RedirectRoute('/tags/<tag_name>/edit', handlers.EditTagHandler, name='edit-tag', strict_slash=True),
    RedirectRoute('/tags/<tag_name>/<action>', handlers.TagHandler, name='show-tag', strict_slash=True),
    RedirectRoute('/rest/', rest.Dispatcher, name='rest', strict_slash=True),
    RedirectRoute('/dashboard/', handlers.DashboardHandler, name='dashboard', strict_slash=True)
]

def get_routes():
    return _routes

def add_routes(app):
    if app.debug:
        secure_scheme = 'http'
    for r in _routes:
        app.router.add(r)
